//
// Created by Karpov Oleg on 1/24/19.
//

#include "CLogReader.h"

#include <stdlib.h>
#include <android/log.h>

CLogReader* CLogReader::sInstance = 0;
sig_atomic_t CLogReader::sInitialized = 0;

CLogReader::~CLogReader(){
    free(mFilter);
}

bool CLogReader::SetFilter(const char *filter){
    mFilter = strdup(filter);

    __android_log_print(ANDROID_LOG_VERBOSE, "CLogReader", "New filter is %s", mFilter);

    return mFilter != NULL;
}

bool CLogReader::AddSourceBlock(const char* block, const size_t block_size){
    if(mFilter != 0 &&
            strlen(mFilter) > 0){
        return true;
    }

    return true;
}

CLogReader* CLogReader::Instance(){
    if(sInitialized == 0){
        sInitialized = 1;
        sInstance = new CLogReader();
    }

    return sInstance;
}

void CLogReader::Release() {
    if(sInitialized){
        sInitialized = 0;
        delete sInstance;
    }
}
