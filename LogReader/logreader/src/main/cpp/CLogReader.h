//
// Created by Karpov Oleg on 1/24/19.
//

#ifndef LOGREADER_CLOGREADER_H
#define LOGREADER_CLOGREADER_H


#include <cwchar>

class CLogReader {
public:
    static CLogReader* Instance();
    void Release();

    bool    SetFilter(const char *filter);   // установка фильтра строк, false - ошибка
    bool    AddSourceBlock(const char* block, const size_t block_size); // добавление очередного блока текстового файла

private:
    CLogReader(){}
    ~CLogReader();

    static CLogReader* sInstance;
    static sig_atomic_t sInitialized;

    char* mFilter = 0;
};


#endif //LOGREADER_CLOGREADER_H
