#include <jni.h>
#include <string>
#include "CLogReader.h"

extern "C"
JNIEXPORT
jboolean JNICALL Java_com_test_logreader_LogReaderLib_addSourceBlock(JNIEnv *env, jobject, jbyteArray block, jsize block_size) {
    jbyte* buffer = new jbyte[block_size];
    env->GetByteArrayRegion(block, 0, block_size, buffer);

    jboolean result =
            (jboolean) CLogReader::Instance()->AddSourceBlock((const char*)buffer, (const size_t)block_size);

    delete [] buffer;

    return result;
}

JNIEXPORT
jboolean JNICALL Java_com_test_logreader_LogReaderLib_setFilter(JNIEnv *env, jobject, jbyteArray filter, jsize filter_size) {
    jbyte buffer[filter_size + 1];
    env->GetByteArrayRegion(filter, 0, filter_size, buffer);
    buffer[filter_size] = '\0';
    return (jboolean) CLogReader::Instance()->SetFilter((const char*)buffer);
}