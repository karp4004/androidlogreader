package com.test.logreader.data.repository;

import android.support.annotation.NonNull;

import com.test.logreader.domain.RestoreableEntity;
import com.test.logreader.domain.model.LogPage;
import com.test.logreader.domain.model.NavigateDirection;
import com.test.logreader.domain.model.RestoreModel;

import java.io.FileNotFoundException;

import io.reactivex.Single;

import static com.test.logreader.domain.model.NavigateDirection.NAVIGATE_NEXT;
import static com.test.logreader.domain.model.NavigateDirection.NAVIGATE_PREVIOUS;

/**
 * @author Karpov Oleg
 */
public class LogRepository implements RestoreableEntity {

    @NonNull
    private final PagedFileReader mFileStorage;

    public LogRepository(@NonNull PagedFileReader fileStorage){
        mFileStorage = fileStorage;
    }

    public void openFile(@NonNull String url) throws IllegalAccessException, FileNotFoundException{
        mFileStorage.openFile(url);
    }

    public Single<LogPage> getPage(@NavigateDirection int direction, int lineCount){
        switch (direction){
            case NAVIGATE_NEXT:
                return Single.fromCallable(()->mFileStorage.readFileNext(lineCount));
            case NAVIGATE_PREVIOUS:
                return Single.fromCallable(()->mFileStorage.readFilePrevious(lineCount));
        }

        return Single.fromCallable(()->mFileStorage.readFileCurrent(lineCount));
    }

    @Override
    public RestoreModel getSavedState() {
        return mFileStorage.getSavedState();
    }

    @Override
    public void onRestoreState(RestoreModel restoreModel) {
        mFileStorage.onRestoreState(restoreModel);
    }
}
