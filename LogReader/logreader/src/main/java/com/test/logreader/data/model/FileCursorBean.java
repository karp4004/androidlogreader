package com.test.logreader.data.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.test.logreader.domain.interactor.LogInteractor.PAGE_SIZE;

/**
 * @author Karpov Oleg
 */
public class FileCursorBean implements Serializable {

    private static final int MAX_CURSOR_LENGTH = PAGE_SIZE * 20;

    private List<Long> mFilePointers = new ArrayList<>();
    private int mCursorIterator;
    private long mCachedPointer;

    public void moveNext(){
        mCursorIterator++;
        if(mCursorIterator >= mFilePointers.size()){
            if(!mFilePointers.isEmpty()) {
                mCursorIterator = mFilePointers.size() - 1;
            } else {
                mCursorIterator = 0;
            }
        }
    }

    public void movePrevious(){
        mCursorIterator--;
        if(mCursorIterator < 0){
            mCursorIterator = 0;
        }
    }

    public void addFirstPointer(long pointer, long limit){
        if(mFilePointers.isEmpty() &&
                isPointerValid(pointer, limit)) {
            mFilePointers.add(pointer);
        }
    }

    public void addCachedPointerAsNext(long limit){
        if(isPointerValid(mCachedPointer, limit)) {
            mFilePointers.add(mCachedPointer);
        }
    }

    public long getCurrentFilePointer(){
        if(mCursorIterator < 0 || mCursorIterator >= mFilePointers.size()){
            mCursorIterator = 0;
        }

        if(isPointerValid(mCursorIterator, mFilePointers.size())) {
            return mFilePointers.get(mCursorIterator);
        }

        return -1;
    }

    public void cachePointer(long pointer){
        mCachedPointer = pointer;
    }

    public boolean isNewPage(){
        long pointer = -1;
        if(!mFilePointers.isEmpty()){
            pointer = mFilePointers.get(mFilePointers.size()-1);
        }

        return pointer < mCachedPointer;
    }

    public boolean isCursorLengthOverflow(){
        return mFilePointers.size() > MAX_CURSOR_LENGTH;
    }

    public void resetCursor(){
        if(!mFilePointers.isEmpty()) {
            mFilePointers = mFilePointers.subList(mFilePointers.size()-1, mFilePointers.size());
            mCursorIterator = 0;
            mCachedPointer = getCurrentFilePointer();
        }
    }

    private boolean isPointerValid(long pointer, long limit){
        return pointer >= 0 && pointer < limit;
    }
}
