package com.test.logreader.domain;

import com.test.logreader.domain.model.RestoreModel;

/**
 * @author Karpov Oleg
 */
public interface RestoreableEntity {
    RestoreModel getSavedState();
    void onRestoreState(RestoreModel restoreModel);
}
