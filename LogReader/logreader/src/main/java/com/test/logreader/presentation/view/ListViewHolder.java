package com.test.logreader.presentation.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.test.logreader.R;

/**
 * @author Karpov Oleg
 */
public class ListViewHolder extends RecyclerView.ViewHolder {
    public ListViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public void bind(String item){
        TextView textView = itemView.findViewById(R.id.list_item_text_view);
        textView.setText(item);
    }
}
