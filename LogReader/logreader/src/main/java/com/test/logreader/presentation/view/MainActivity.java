package com.test.logreader.presentation.view;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.test.logreader.R;
import com.test.logreader.data.repository.LogRepository;
import com.test.logreader.data.repository.PagedFileReader;
import com.test.logreader.domain.interactor.LogInteractor;
import com.test.logreader.domain.model.LogPage;
import com.test.logreader.domain.model.RestoreModel;
import com.test.logreader.presentation.presenter.LogReaderPresenter;

import static android.widget.Toast.LENGTH_SHORT;

public class MainActivity extends AppCompatActivity implements LogReaderView{

    private static final int PERMISSION_REQUEST_CODE = 11111;
    private static final String RESTORE_MODEL = "RESTORE_MODEL";

    private LogListAdapter mLogListAdapter = new LogListAdapter();
    private RecyclerView mRecyclerView;
    private LogInteractor mLogInteractor = new LogInteractor(
            new LogRepository(
                    new PagedFileReader()
            )
    );

    private LogReaderPresenter mLogReaderPresenter =
            new LogReaderPresenter(this, mLogInteractor);

    private RecyclerViewOverscrollChecker mRecyclerViewOverscrollChecker =
            new RecyclerViewOverscrollChecker(direction->mLogReaderPresenter.onScrollView(direction));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        PERMISSION_REQUEST_CODE);
            }
        }

        if(savedInstanceState != null){
            RestoreModel restoreModel = (RestoreModel)savedInstanceState.getSerializable(RESTORE_MODEL);
            mLogReaderPresenter.onRestoreState(restoreModel);
        }

        setContentView(R.layout.activity_main);

        mRecyclerView = findViewById(R.id.list_view);
        mRecyclerView.setLayoutManager(
                new LinearLayoutManager(mRecyclerView.getContext())
        );
        mRecyclerView.setAdapter(mLogListAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mRecyclerViewOverscrollChecker.onScrolled(recyclerView);
            }
        });

        mRecyclerView.setOnTouchListener(
                (v, event)-> mRecyclerViewOverscrollChecker.onTouch(mRecyclerView, event)
        );

        EditText editText = findViewById(R.id.url_edit_text);
        Button readButton = findViewById(R.id.read_button);
        readButton.setOnClickListener((view)->mLogReaderPresenter.onReadLog(
                    editText.getText().toString()
                )
        );
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        RestoreModel restoreModel = mLogReaderPresenter.getSavedState();
        outState.putSerializable(RESTORE_MODEL, restoreModel);
    }

    @Override
    public void updatePageView(LogPage logPage){
        mLogListAdapter.submitList(logPage.getRecords());
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLogReaderPresenter.onDestroy();
    }
}
