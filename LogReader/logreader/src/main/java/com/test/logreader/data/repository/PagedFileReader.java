package com.test.logreader.data.repository;

import android.os.Environment;
import android.support.annotation.NonNull;

import com.test.logreader.LogReaderLib;
import com.test.logreader.data.model.FileCursorBean;
import com.test.logreader.domain.RestoreableEntity;
import com.test.logreader.domain.model.LogPage;
import com.test.logreader.domain.model.RestoreModel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Karpov Oleg
 */
public class PagedFileReader implements RestoreableEntity {

    private static final String FILE_CURSOR_RESTORABLE_PARAMETER = "FILE_CURSOR_RESTORABLE_PARAMETER";

    private RandomAccessFile mRandomAccessFile;
    private FileCursorBean mFileCursorBean = new FileCursorBean();

    public void openFile(@NonNull String url) throws IllegalAccessException, FileNotFoundException{
        if(mRandomAccessFile == null) {
            if(isExternalStorageReadable()) {
                File file = new File(url);
                if (file.exists()) {
                    if (file.canRead()) {
                        mRandomAccessFile = new RandomAccessFile(file, "r");
                    } else {
                        throw new IllegalAccessException("file: " + url + " not exists");
                    }
                } else {
                    throw new IllegalAccessException("Can't read file: " + url);
                }
            } else {
                throw new IllegalAccessException("External storage unavailable");
            }
        }
    }

    public LogPage readFileNext(int lineCount) throws SecurityException, IOException{

        mFileCursorBean.moveNext();
        List<String> list = readFile(lineCount);

        boolean isNewPage = addNewPagePointer();
        return new LogPage(list, isNewPage);
    }

    public LogPage readFileCurrent(int lineCount) throws SecurityException, IOException{

        mFileCursorBean.addFirstPointer(
                mRandomAccessFile.getFilePointer(), mRandomAccessFile.length()
        );
        List<String> list = readFile(lineCount);

        boolean isNewPage = addNewPagePointer();
        return new LogPage(list, isNewPage);
    }

    public LogPage readFilePrevious(int lineCount) throws SecurityException, IOException{

        mFileCursorBean.movePrevious();
        return new LogPage(readFile(lineCount), false);
    }

    private List<String> readFile(int lineCount) throws SecurityException, IOException{
        seekFileCursor();
        List<String> lines = new ArrayList<>();

        int index = 0;
        while(lines.size() < lineCount) {
            String line = mRandomAccessFile.readLine();
            if(line != null) {
                byte[] bytes = line.getBytes();
                if(LogReaderLib.addSourceBlock(bytes, bytes.length)) {
                    if(index % 3 == 0) {
                        lines.add(line);
                    }

                    index++;
                }
            } else {
                break;
            }

            if(lines.size() == lineCount/2){
                mFileCursorBean.cachePointer(
                        mRandomAccessFile.getFilePointer()
                );
            }
        }

        return lines;
    }

    private boolean addNewPagePointer() throws IOException{
        boolean isNewPage = mFileCursorBean.isNewPage();
        if(isNewPage){
            mFileCursorBean.addCachedPointerAsNext(mRandomAccessFile.length());
            if(mFileCursorBean.isCursorLengthOverflow()){
                mFileCursorBean.resetCursor();
            }
        }

        return isNewPage;
    }

    private boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }

    private void seekFileCursor() throws IOException{
        long cursor = mFileCursorBean.getCurrentFilePointer();
        if (cursor >= 0 && cursor < mRandomAccessFile.length()) {
            mRandomAccessFile.seek(cursor);
        }
    }

    @Override
    public RestoreModel getSavedState() {
        RestoreModel restoreModel = new RestoreModel();
        restoreModel.saveParameter(FILE_CURSOR_RESTORABLE_PARAMETER, mFileCursorBean);
        return restoreModel;
    }

    @Override
    public void onRestoreState(RestoreModel restoreModel) {
        FileCursorBean fileCursorBean = (FileCursorBean)restoreModel.restoreParameter(FILE_CURSOR_RESTORABLE_PARAMETER);
        if(fileCursorBean != null){
            mFileCursorBean = fileCursorBean;
        }
    }
}
