package com.test.logreader.presentation.view;

import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.logreader.R;

/**
 * @author Karpov Oleg
 */
public class LogListAdapter extends ListAdapter<String, ListViewHolder> {

    public LogListAdapter() {
        super(mDiffCallback);
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder viewHolder, int position) {
        viewHolder.bind(getItem(position));
    }

     private static final DiffUtil.ItemCallback<String> mDiffCallback =
            new DiffUtil.ItemCallback<String>() {
                @Override
                public boolean areItemsTheSame(
                        @NonNull String oldUser, @NonNull String newUser) {
                    return oldUser.equals(newUser);
                }
                @Override
                public boolean areContentsTheSame(
                        @NonNull String oldUser, @NonNull String newUser) {
                    return oldUser.equals(newUser);
                }
            };
}
