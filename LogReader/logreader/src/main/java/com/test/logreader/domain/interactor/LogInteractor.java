package com.test.logreader.domain.interactor;

import android.support.annotation.NonNull;
import android.util.Log;

import com.test.logreader.data.repository.LogRepository;
import com.test.logreader.domain.RestoreableEntity;
import com.test.logreader.domain.model.LogPage;
import com.test.logreader.domain.model.NavigateDirection;
import com.test.logreader.domain.model.RestoreModel;

import java.io.FileNotFoundException;

import io.reactivex.Single;

import static com.test.logreader.domain.model.NavigateDirection.NAVIGATE_CURRENT;

/**
 * @author Karpov Oleg
 */
public class LogInteractor implements RestoreableEntity {

    public static final int PAGE_SIZE = 50;
    private static final String URL_RESTORABLE_PARAMETER = "URL_RESTORABLE_PARAMETER";

    private String mUrl;

    @NonNull
    private LogRepository mLogRepository;

    public LogInteractor(@NonNull LogRepository logRepository){
        mLogRepository = logRepository;
    }

    public void setUrl(@NonNull String url) throws SecurityException, IllegalArgumentException{
        mUrl = url;
    }

    public Single<LogPage> getPage(@NavigateDirection int direction){
        return mLogRepository.getPage(direction, PAGE_SIZE)
                .doOnSubscribe(disposable-> openFile())
                .doOnSuccess(succes->saveResult(succes));
    }

    private void saveResult(LogPage logPage){
        Log.d("LogInteractor", "logPage: " + logPage);
    }

    private void openFile() throws FileNotFoundException, IllegalAccessException{
        if(mUrl != null) {
            mLogRepository.openFile(mUrl);
        } else {
            throw new FileNotFoundException("URL must not be null");
        }
    }

    @Override
    public RestoreModel getSavedState() {
        RestoreModel restoreModel = mLogRepository.getSavedState();
        if(restoreModel != null) {
            restoreModel.saveParameter(URL_RESTORABLE_PARAMETER, mUrl);
        }

        return restoreModel;
    }

    @Override
    public void onRestoreState(RestoreModel restoreModel) {
        mLogRepository.onRestoreState(restoreModel);

        if(restoreModel != null) {
            mUrl = (String)restoreModel.restoreParameter(URL_RESTORABLE_PARAMETER);
        }
    }
}
