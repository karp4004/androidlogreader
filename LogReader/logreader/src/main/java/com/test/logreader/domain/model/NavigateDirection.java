package com.test.logreader.domain.model;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;

import static com.test.logreader.domain.model.NavigateDirection.NAVIGATE_CURRENT;
import static com.test.logreader.domain.model.NavigateDirection.NAVIGATE_NEXT;
import static com.test.logreader.domain.model.NavigateDirection.NAVIGATE_PREVIOUS;
import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * @author Karpov Oleg
 */
@Retention(SOURCE)
@IntDef({
        NAVIGATE_CURRENT,
        NAVIGATE_NEXT,
        NAVIGATE_PREVIOUS
})
public @interface NavigateDirection {
    int NAVIGATE_CURRENT = 0;
    int NAVIGATE_NEXT = 1;
    int NAVIGATE_PREVIOUS = -1;
}