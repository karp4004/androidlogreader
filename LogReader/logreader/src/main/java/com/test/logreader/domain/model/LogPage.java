package com.test.logreader.domain.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Karpov Oleg
 */
public final class LogPage {
    private List<String> mRecords = new ArrayList<>();
    private boolean mNewPage;

    public LogPage(List<String> record,
                   boolean newPage) {
        if(record != null) {
            mRecords.clear();
            mRecords.addAll(record);
        }

        mNewPage = newPage;
    }

    public List<String> getRecords() {
        return mRecords;
    }

    public boolean isNewPage() {
        return mNewPage;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("LogPage{");
        sb.append("mRecords=").append(mRecords);
        sb.append(", mNewPage=").append(mNewPage);
        sb.append('}');
        return sb.toString();
    }
}
