package com.test.logreader;

/**
 * @author Karpov Oleg
 */
public class LogReaderLib {

    static {
        System.loadLibrary("log_reader_lib");
    }

    public static native boolean addSourceBlock(byte[] block, int block_size);
    public static native boolean setFilter(byte[] filter, int filter_size);
}
