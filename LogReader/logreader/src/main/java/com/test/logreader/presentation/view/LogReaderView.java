package com.test.logreader.presentation.view;

import com.test.logreader.domain.model.LogPage;

/**
 * @author Karpov Oleg
 */
public interface LogReaderView {
    void updatePageView(LogPage logPage);
    void showMessage(String message);
}
