package com.test.logreader.presentation.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;

import com.test.logreader.domain.model.NavigateDirection;

import static android.support.v7.widget.RecyclerView.SCROLL_STATE_SETTLING;
import static android.view.MotionEvent.ACTION_DOWN;
import static android.view.MotionEvent.ACTION_MOVE;
import static android.view.MotionEvent.ACTION_UP;
import static com.test.logreader.domain.model.NavigateDirection.NAVIGATE_NEXT;
import static com.test.logreader.domain.model.NavigateDirection.NAVIGATE_PREVIOUS;

/**
 * @author Karpov Oleg
 */
public class RecyclerViewOverscrollChecker {

    private static final float OVERSCROLL_THRESHOLD = 20.f;

    private float touchYDown;
    private boolean overScrollChecking;

    @NonNull
    private final RecyclerViewOverscroll mRecyclerViewOverscroll;

    public RecyclerViewOverscrollChecker(@NonNull RecyclerViewOverscroll recyclerViewOverscroll){
        mRecyclerViewOverscroll = recyclerViewOverscroll;
    }

    public boolean onTouch(@NonNull RecyclerView recyclerView, MotionEvent event){
        if (event.getAction() == ACTION_DOWN) {
            touchYDown = event.getY();
            overScrollChecking = false;
        } else if (event.getAction() == ACTION_MOVE) {
            float distance = touchYDown - event.getY();
            if(Math.abs(distance) > OVERSCROLL_THRESHOLD && !overScrollChecking){
                if(!recyclerView.canScrollVertically(NAVIGATE_NEXT) && distance > 0){
                    overScrollChecking = true;
                    mRecyclerViewOverscroll.onOverscroll(NAVIGATE_NEXT);
                } else if(!recyclerView.canScrollVertically(NAVIGATE_PREVIOUS) && distance < 0){
                    overScrollChecking = true;
                    mRecyclerViewOverscroll.onOverscroll(NAVIGATE_PREVIOUS);
                }
            }
        } else if (event.getAction() == ACTION_UP) {
            recyclerView.performClick();
        }

        return false;
    }

    public void onScrolled(@NonNull RecyclerView recyclerView){
        if(recyclerView.getScrollState() == SCROLL_STATE_SETTLING) {
            if(!overScrollChecking) {
                if (!recyclerView.canScrollVertically(NAVIGATE_NEXT)) {
                    overScrollChecking = true;
                    mRecyclerViewOverscroll.onOverscroll(NAVIGATE_NEXT);
                } else if (!recyclerView.canScrollVertically(NAVIGATE_PREVIOUS)) {
                    overScrollChecking = true;
                    mRecyclerViewOverscroll.onOverscroll(NAVIGATE_PREVIOUS);
                }
            }
        }
    }

    public interface RecyclerViewOverscroll{
        void onOverscroll(@NavigateDirection int direction);
    }
}
