package com.test.logreader.domain.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Karpov Oleg
 */
public class RestoreModel implements Serializable {
    private Map<String, Serializable> mSerializableMap = new HashMap<>();

    public void saveParameter(String name, Serializable value){
        mSerializableMap.put(name, value);
    }

    public Serializable restoreParameter(String name){
        return mSerializableMap.get(name);
    }
}
