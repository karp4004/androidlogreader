package com.test.logreader.presentation.presenter;

import android.support.annotation.NonNull;

import com.test.logreader.domain.RestoreableEntity;
import com.test.logreader.domain.interactor.LogInteractor;
import com.test.logreader.domain.model.NavigateDirection;
import com.test.logreader.domain.model.RestoreModel;
import com.test.logreader.presentation.view.LogReaderView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.test.logreader.domain.model.NavigateDirection.NAVIGATE_CURRENT;
import static com.test.logreader.domain.model.NavigateDirection.NAVIGATE_NEXT;
import static com.test.logreader.domain.model.NavigateDirection.NAVIGATE_PREVIOUS;

/**
 * @author Karpov Oleg
 */
public class LogReaderPresenter implements RestoreableEntity {

    private static final String ONCE_LOADED_RESTORABLE_PARAMETER = "ONCE_LOADED_RESTORABLE_PARAMETER";

    private LogReaderView mLogReaderView;
    private LogInteractor mLogInteractor;
    private boolean mLoading;
    private Boolean mOnceLoaded;

    private final CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    public LogReaderPresenter(@NonNull LogReaderView logReaderView,
                              @NonNull LogInteractor logInteractor){
        mLogReaderView = logReaderView;
        mLogInteractor = logInteractor;
    }

    public void onScrollView(@NavigateDirection int direction){
        getPage(direction);
    }

    public void onReadLog(String uri){
        mLogInteractor.setUrl(uri);
        getPage(NAVIGATE_CURRENT);
    }

    public void onDestroy(){
        mCompositeDisposable.dispose();
    }

    private void getPage(@NavigateDirection int direction){
        if(!mLoading) {
            mLoading = true;
            mCompositeDisposable.add(mLogInteractor
                    .getPage(direction)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            success -> {
                                mOnceLoaded = true;
                                mLoading = false;
                                mLogReaderView.updatePageView(success);

                                String page = "Current";

                                switch (direction){
                                    case NAVIGATE_NEXT:
                                        page = "Next";
                                        break;

                                    case NAVIGATE_PREVIOUS:
                                        page = "Previous";
                                        break;
                                }

                                mLogReaderView.showMessage(page + " page is loaded");
                            },
                            fail -> {
                                mLoading = false;

                                String message =
                                        fail.getMessage() != null && !fail.getMessage().isEmpty()?
                                                fail.getMessage():
                                                "Unknown error";

                                mLogReaderView.showMessage(message);
                            }
                    )
            );
        }
    }

    @Override
    public RestoreModel getSavedState() {
        RestoreModel restoreModel = mLogInteractor.getSavedState();
        if(restoreModel != null) {
            restoreModel.saveParameter(ONCE_LOADED_RESTORABLE_PARAMETER, mOnceLoaded);
        }

        return restoreModel;
    }

    @Override
    public void onRestoreState(RestoreModel restoreModel) {
        mLogInteractor.onRestoreState(restoreModel);

        if(restoreModel != null) {
            mOnceLoaded = (Boolean)restoreModel.restoreParameter(ONCE_LOADED_RESTORABLE_PARAMETER);

            if(mOnceLoaded != null && mOnceLoaded) {
                getPage(NAVIGATE_CURRENT);
            }
        }
    }
}
